var config                   = require('./config');
var passport                 = require('passport');
var BasicStrategy            = require('passport-http').BasicStrategy;
var ClientPasswordStrategy   = require('passport-oauth2-client-password').Strategy;
var BearerStrategy           = require('passport-http-bearer').Strategy;
var FacebookStrategy         = require('passport-facebook').Strategy;
var UserModel                = require('./mongoose').UserModel;
var ClientModel              = require('./mongoose').ClientModel;
var AccessTokenModel         = require('./mongoose').AccessTokenModel;
var RefreshTokenModel        = require('./mongoose').RefreshTokenModel;
var oauth2                   = require('./oauth2');

passport.use(new BasicStrategy(function(email, password, done) {
    ClientModel.findOne({
        clientId: email
    }, function(err, client) {
        if (err) {
            return done(err);
        }
        if (!client) {
            return done(null, false);
        }
        if (client.clientSecret != password) {
            return done(null, false);
        }
        return done(null, client);
    });
}));
passport.use(new ClientPasswordStrategy(function(clientId, clientSecret, done) {
    ClientModel.findOne({
        clientId: clientId
    }, function(err, client) {
        if (err) {
            return done(err);
        }
        if (!client) {
            return done(null, false);
        }
        if (client.clientSecret != clientSecret) {
            return done(null, false);
        }
        return done(null, client);
    });
}));
passport.use(new BearerStrategy(function(accessToken, done) {
    AccessTokenModel.findOne({
        token: accessToken
    }, function(err, token) {
        if (err) {
            return done(err);
        }
        if (!token) {
            return done(null, false);
        }
        if (Math.round((Date.now() - token.created) / 1000) > config.get('security:tokenLife')) {
            AccessTokenModel.remove({
                token: accessToken
            }, function(err) {
                if (err) return done(err);
            });
            return done(null, false, {
                message: 'Token expired'
            });
        }
        UserModel.findById(token.userId, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, {
                    message: 'Unknown user'
                });
            }
            var info = {
                scope: '*'
            }
            done(null, user, info);
        });
    });
}));
passport.use(new FacebookStrategy({
    clientID: '748477285229136',
    clientSecret: '756d90e0c1287c551c6cdc02e92bb455',
    callbackURL: "http://localhost:1337/auth/facebook/callback",
    profileFields: ['id', 'emails', 'displayName']
}, function(accessToken, refreshToken, profile, done) {
    // asynchronous verification, for effect...
    process.nextTick(function() {
        var user = UserModel.findOne({
            email: profile._json.email
        }, function(err, user) {
            if(!user) {
                user = UserModel({
                    email: profile._json.email,
                    username: {
                        first_name: profile._json.name.split(' ')[0],
                        last_name: profile._json.name.split(' ')[2]
                    },
                    password: '123456'
                })
                user.save(function(err, data) {
                    if (!err) {
                        console.log("user created");
                        var modelData = {
                            userId: user._id,
                            clientId: 'mobileV1'
                        };
                        var tokens = oauth2.generateTokens(modelData, done);
                        passport.serializeUser(function(user, done) {
                            
                            done(null, user, tokens);
                        });
                    } else {
                        console.log(err);
                        if (err.name == 'ValidationError') {
                            res.statusCode = 400;
                            res.send({
                                error: 'Validation error'
                            });
                        } else {
                            res.statusCode = 500;
                            res.send({
                                error: 'Server error'
                            });
                        }
                        log.error('Internal error(%d): %s', res.statusCode, err.message);
                    }
                })
            } else {
                passport.serializeUser(function(user, done) {
                    done(null, user);
                });
                return done(null, user);
            }
        });
    });
}));