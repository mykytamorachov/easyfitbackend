var mongoose     = require('mongoose');
var log          = require('./log')(module);
var config       = require('./config');
var crypto       = require('crypto');

mongoose.connect(config.get('mongoose:uri'));
var db           = mongoose.connection;

db.on('error', function(err) {
    log.error('connection error:', err.message);
});
db.once('open', function callback() {
    log.info("Connected to DB!");
});

var Schema = mongoose.Schema;

var workouts = new Schema({
    workout_id: Schema.Types.ObjectId,
    workout_name: String,
    workout_author: String,
    workout_created_date: {
        type: Date,
        default: Date.now
    },
    workout_trainings: [{
        type: Schema.Types.ObjectId,
        ref: 'Trainings'
    }]
});

var trainings = new Schema({
    training_name: String,
    training_id: Schema.Types.ObjectId,
    is_favourite: Boolean,
    training_type: String,
    training_excersizes: [{
        name: String,
        sets: Number,
        repeats: [Number],
        set_type: String,
        pause: Number
    }]
});

// User
var User = new Schema({
    username: {
        first_name:{
            type:String
        },
        last_name:{
            type:String
        }
    },
    email:{
        type:String,
        unique: true,
        required:true
    },
    userData:{},
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    workouts: [{
        type: Schema.Types.ObjectId,
        ref: 'Workouts'
    }]
});

User.methods.encryptPassword = function(password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
    //more secure - return crypto.pbkdf2Sync(password, this.salt, 10000, 512);
};

User.virtual('userId').get(function() {
    return this.id;
});

User.virtual('user_workouts').get(function() {
    return this.workouts;
});

User.virtual('password').set(function(password) {
    this._plainPassword = password;
    this.salt = crypto.randomBytes(32).toString('base64');
    //more secure - this.salt = crypto.randomBytes(128).toString('base64');
    this.hashedPassword = this.encryptPassword(password);
}).get(function() {
    return this._plainPassword;
});

User.methods.checkPassword = function(password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

var UserModel = mongoose.model('User', User);

// Client
var Client = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    clientId: {
        type: String,
        unique: true,
        required: true
    },
    clientSecret: {
        type: String,
        required: true
    }
});

var ClientModel = mongoose.model('Client', Client);

// AccessToken
var AccessToken = new Schema({
    userId: {
        type: String,
        required: true
    },
    clientId: {
        type: String,
        required: true
    },
    token: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

var AccessTokenModel = mongoose.model('AccessToken', AccessToken);

// RefreshToken
var RefreshToken = new Schema({
    userId: {
        type: String,
        required: true
    },
    clientId: {
        type: String,
        required: true
    },
    token: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

var RefreshTokenModel = mongoose.model('RefreshToken', RefreshToken);
var Workouts = mongoose.model('Workouts', workouts);
var Trainings = mongoose.model('Trainings', trainings);

var exports = {
    Workouts: Workouts,
    Trainings: Trainings,
    mongoose: mongoose,
    UserModel: UserModel,
    ClientModel: ClientModel,
    AccessTokenModel:AccessTokenModel,
    RefreshTokenModel: RefreshTokenModel
};

module.exports = exports;