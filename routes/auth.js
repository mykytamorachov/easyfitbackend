var express = require('express');
var router = express.Router();
var passport = require('passport');
var mongoose = require('mongoose');
var log = require('./../libs/log')(module);
var UserModel = require('./../libs/mongoose').UserModel;
var AccessTokenModel = require('./../libs/mongoose').AccessTokenModel;
var RefreshTokenModel = require('./../libs/mongoose').RefreshTokenModel;
var ClientModel = require('./../libs/mongoose').ClientModel;
var config = require('./../libs/config');
var oauth2 = require('./../libs/oauth2');

router.post('/token', oauth2.token);

router.post('/register', function(req, res, next) {
    var user = new UserModel(req.body);
    user.save(function(err, data) {
        if (!err) {
            log.info("user created");
            var client = new ClientModel({ name: "OurService iOS client v1", clientId: "mobileV1", clientSecret:"abc123456" });
            client.save(function(err, client) {
                if(err) return log.error(err);
                else log.info("New client - %s:%s",client.clientId,client.clientSecret);
            });
            return res.send({
                status: 'OK'
            });
        } else {
            console.log(err);
            if (err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({
                    error: 'Validation error'
                });
            } else {
                res.statusCode = 500;
                res.send({
                    error: 'Server error'
                });
            }
            log.error('Internal error(%d): %s', res.statusCode, err.message);
        }
    })
});

router.get('/facebook', passport.authenticate('facebook', {
    scope: ['email'],
    successRedirect: '/home',
    failureRedirect: '/login'
}));

router.get('/facebook/callback', passport.authenticate('facebook', {
    failureRedirect: '/login'
}), function(req, res) {
    var tokens = {};
    AccessTokenModel.findOne({
        userId: req.user._id
    }, function(err, access_token) {
        if (!err) {
            res.status(401);
            res.send({error:"Not Authorized"});
            return;
        } else {
            tokens.access_token = access_token.token;
            RefreshTokenModel.findOne({
                    userId: req.user._id
                }, function(err, refresh_token) {
                    tokens.refresh_token = refresh_token.token;
                    res.send({
                        user: {
                            user_id: req.user._id,
                            email: req.user.email,
                            username: req.user.username,
                            tokens: tokens
                        }
                    });
                    console.log(tokens);
                
            });
        }
    });
});
module.exports = router;