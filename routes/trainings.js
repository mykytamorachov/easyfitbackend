var express = require('express');
var router = express.Router();
var mongoose    = require('mongoose');
var passport = require('passport');
var log             = require('./../libs/log')(module);
var WorkoutsModel = require('./../libs/mongoose').Workouts;
var TrainingsModel = require('./../libs/mongoose').Trainings;
var config      = require('./../libs/config');

router.get('/', passport.authenticate('bearer', { session: false }), function (req, res) {
    return TrainingsModel.find(function (err, trainings) {
        if (!err) {
            return res.send({ status: 'OK', trainings:trainings });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

router.get('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    return TrainingsModel.findById(req.params.id, function (err, trainings) {
        if(!workouts) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', trainings:trainings });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

router.post('/add', passport.authenticate('bearer', { session: false }), function (req, res) {

	var training = new TrainingsModel({
		training_name: req.body.training_name,
	    is_favourite: false,
    	training_type: req.body.training_type,
	});



	training.save(function (err) {
        if (!err) {
            log.info("workout created");
            // return res.send({ status: 'OK', training:training });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
    
    //hardcoded!!! change to real data!!!
    var workout = WorkoutsModel.findById('54a46eccf046844c9f54f807',function (err, workout)
    {
    	if (!err){
    		workout.workout_trainings.push(training._id);
    		workout.save(function (err, workouts){
    			return res.send({ status: 'OK', workout:workouts });
    		})
    	}
    })
});

module.exports = router;