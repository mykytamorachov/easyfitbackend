var express = require('express');
var router = express.Router();
var passport = require('passport');
var mongoose    = require('mongoose');
var log             = require('./../libs/log')(module);
var WorkoutsModel = require('./../libs/mongoose').Workouts;
var TrainingsModel = require('./../libs/mongoose').Trainings;
var UserModel = require('./../libs/mongoose').UserModel;
var config      = require('./../libs/config');




router.get('/', passport.authenticate('bearer', { session: false }), function (req, res) {
    return WorkoutsModel.findOne(function (err, workouts) {
        if (!err) {
            return res.send({ status: 'OK', workouts:workouts });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

router.get('/:id', passport.authenticate('bearer', { session: false }), function (req, res) {
    return WorkoutsModel.findById(req.params.id, function (err, workouts) {
        if(!workouts) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
        	if(workouts.workout_trainings)
        	{
        		var trainings = workouts.workout_trainings;
        		var ids = [];
        		for (var i=0; i<trainings.length; i++)
        		{
        			ids.push(trainings[i])
        		}
        		TrainingsModel.find({
				    '_id': { $in: ids}
				}, function(err, trainings){
				     return res.send({ status: 'OK', workout:workouts, trainings: trainings});
				});
        		
        	}
        	else{
        		return res.send({ status: 'OK', workout:workouts });
        	}
            
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

router.post('/add',passport.authenticate('bearer', { session: false }), function (req, res) {

	var workout = new WorkoutsModel({
		workout_name: req.body.workout_name,
	    workout_author: req.body.workout_author
	});

	workout.save(function (err) {
        if (!err) {
            log.info("workout created");
            var user = UserModel.find( { _id: req.body.user_id } );
            user.workouts.push(workout._id);
            user.save(function(error){
                if(!error)
                    {log.info("user updated")}
                else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
            })
            return res.send({ status: 'OK', workout:workout });
        } else {
            console.log(err);
            if(err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s',res.statusCode,err.message);
        }
    });
});

module.exports = router;
