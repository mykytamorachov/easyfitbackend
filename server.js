var express            = require('express');
var favicon            = require('serve-favicon');
var path               = require('path');
var passport           = require('passport');
var bodyParser         = require('body-parser');
var config             = require('./libs/config');
var log                = require('./libs/log')(module);
var oauth2             = require('./libs/oauth2');
var WorkoutsModel      = require('./libs/mongoose').Workouts;
var UserModel          = require('./libs/mongoose').UserModel;
var AccessTokenModel   = require('./libs/mongoose').AccessTokenModel;
var RefreshTokenModel  = require('./libs/mongoose').RefreshTokenModel;
var workouts           = require('./routes/workouts');
var trainings          = require('./routes/trainings');
var auth               = require('./routes/auth');
var cors               = require('cors');
var app                = express();

require('./libs/auth');



app.use(cors());
app.use(passport.initialize());
app.get('/api/userInfo', passport.authenticate('bearer', {
    session: false
}), function(req, res) {
    res.json({
        user_id: req.user.userId,
        username: req.user.username,
        email: req.user.email
    })
});
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use('/workouts', workouts);
app.use('/trainings', trainings);
app.use('/auth', auth);
app.use(express.static(path.join(__dirname, "public")));
app.use(function(req, res, next) {
    res.status(404);
    log.debug('Not found URL: %s', req.url);
    res.send({
        error: 'Not found'
    });
    return;
});
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    log.error('Internal error(%d): %s', res.statusCode, err.message);
    res.send({
        error: err.message
    });
    return;
});




app.listen(config.get('port'), function() {
    log.info('Express server listening on port ' + config.get('port'));
});