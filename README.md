Для работы необходимо выполнить npm install в директории, 
запустить mongod, 
node dataGen.js (дождаться выполнения), 
а затем node server.js.

Example requests:

http POST http://localhost:1337/oauth/token grant_type=password client_id=mobileV1 client_secret=abc123456 username=andrey password=simplepassword

http POST http://localhost:1337/auth/token grant_type=refresh_token client_id=mobileV1 client_secret=abc123456 refresh_token=o5NHFrCpjozrCDED2NnC+fmiDoJLeLMrrW+/Kk3NGh8=

http http://localhost:1337/api/userinfo Authorization:'Bearer TOKEN'


http://localhost:1337/api/userinfo Authorization:"Bearer uFyHxCxrUmpU89FIO8NBput1oC2SwjLJo/ovB0zfcdA="
http://localhost:1337/api/userinfo Authorization:'Bearer rDfw3/UJcphYHjsMuuZ+pRQ0YRXK/QdmEpWs7fhurlA='